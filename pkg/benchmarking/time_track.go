package benchmarking

import (
	"time"

	"github.com/rs/zerolog/log"
)

// TimeStamp is a struct to hold the execution time of a function
type TimeStamp struct {
	Instance string `json:"instance"`
	Duration int64  `json:"duration"`
}

// TimeTrack is a function to measure the execution time of a function
func TimeTrack(start time.Time, instance string) TimeStamp {
	elapsed := time.Since(start)
	ts := TimeStamp{
		Duration: elapsed.Nanoseconds(),
		Instance: instance,
	}
	log.Info().Interface("timestamp", ts).Msg("TimeTrack")
	return ts
}
