package fns

import (
	"time"

	"valerius.me/scap/pkg/benchmarking"
)

type Args struct {
	N         int64
	ImageData []byte
}

// FIX: 2024/01/24 12:05:56 rpc.Register: type Fn has no exported methods of suitable type
// Fn is the struct that contains the functions that can be called over rpc
type Fn struct{}

// RPCResponse is a struct that holds the function name and the transformed image data.
type RPCResponse struct {
	FN               string
	TransformedImage []byte
}

// Empty is a method on the Fn struct that performs an operation, benchmarks it, and sets the reply.
// It takes an Args pointer and a RPCResponse pointer as arguments.
// It returns an error if any occurs during the operation.
func (f *Fn) Empty(_ *Args, reply *RPCResponse) error {
	tsStart := time.Now()
	Empty()
	benchmarking.TimeTrack(tsStart, "fns.Empty")
	*reply = RPCResponse{FN: "Empty"}
	return nil
}

// Image is a method on the Fn struct that transforms an image, benchmarks the operation, and sets the reply.
// It takes an Args pointer and a RPCResponse pointer as arguments.
// It returns an error if any occurs during the operation.
func (f *Fn) Image(args *Args, reply *RPCResponse) error {
	tsStart := time.Now()
	transformedBytes, err := TransformImage(args.ImageData)
	if err != nil {
		return err
	}
	benchmarking.TimeTrack(tsStart, "fns.Image")
	*reply = RPCResponse{FN: "Image", TransformedImage: transformedBytes}
	return nil
}

// Math is a method on the Fn struct that performs a mathematical operation, benchmarks it, and sets the reply.
// It takes an Args pointer and a RPCResponse pointer as arguments.
// It returns an error if any occurs during the operation.
func (f *Fn) Math(args *Args, reply *RPCResponse) error {
	tsStart := time.Now()
	benchmarking.TimeTrack(tsStart, "fns.Math")
	res, err := Math(args.N)
	if err != nil {
		return err
	}
	*reply = res
	return nil
}

// Sleep is a method on the Fn struct that performs a sleep operation, benchmarks it, and sets the reply.
// It takes an Args pointer and a RPCResponse pointer as arguments.
// It returns an error if any occurs during the operation.
func (f *Fn) Sleep(_ *Args, reply *RPCResponse) error {
	tsStart := time.Now()
	Sleeper()
	benchmarking.TimeTrack(tsStart, "fns.Sleep")
	*reply = RPCResponse{FN: "Sleep"}
	return nil
}
