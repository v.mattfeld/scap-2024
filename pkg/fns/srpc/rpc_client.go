package srpc

import (
	"net/rpc"
	"time"

	"valerius.me/scap/pkg/fns"

	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/benchmarking"
)

type RpcClient struct {
	client *rpc.Client
}

func NewRpcClient(serverAddress string) *RpcClient {
	client, err := rpc.DialHTTP("tcp", serverAddress)
	if err != nil {
		log.Fatal().Err(err).Msg("dialing")
	}
	return &RpcClient{
		client: client,
	}
}

func (r *RpcClient) Close() {
	r.client.Close()
}

func (r *RpcClient) Empty() (reply fns.RPCResponse) {
	tsStart := time.Now()
	err := r.client.Call("Fn.Empty", &fns.Args{}, &reply)
	benchmarking.TimeTrack(tsStart, "RpcClient.Empty")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to call Fn.Empty")
	}
	return reply
}

func (r *RpcClient) Image(imageData []byte) (reply fns.RPCResponse) {
	tsStart := time.Now()
	err := r.client.Call("Fn.Image", &fns.Args{ImageData: imageData}, &reply)
	benchmarking.TimeTrack(tsStart, "RpcClient.Empty")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to call Fn.Image")
	}
	return reply
}

func (r *RpcClient) Math(n int64) (reply fns.RPCResponse) {
	tsStart := time.Now()
	err := r.client.Call("Fn.Math", &fns.Args{N: n}, &reply)
	benchmarking.TimeTrack(tsStart, "RpcClient.Math")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to call Fn.Math")
	}
	return reply
}

func (r *RpcClient) Sleep() (reply fns.RPCResponse) {
	tsStart := time.Now()
	err := r.client.Call("Fn.Sleep", &fns.Args{}, &reply)
	benchmarking.TimeTrack(tsStart, "RpcClient.Sleep")
	if err != nil {
		log.Fatal().Err(err).Msg("failed to call Fn.Sleep")
	}
	return reply
}
