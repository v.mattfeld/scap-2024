package webserver

import (
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/benchmarking"
	"valerius.me/scap/pkg/fns/srpc"
	"valerius.me/scap/pkg/webserver/util"
)

func Iris(host, rpcServerAddress string, port int) {
	i := iris.New()
	client := srpc.NewRpcClient(rpcServerAddress)

	i.Get(util.EmptyRoute, func(c iris.Context) {
		start := time.Now()
		// 1. Execute the Empty function
		res := client.Empty()
		// 2. Write the response
		benchmarking.TimeTrack(start, "iris.emptyHandler")
		c.StatusCode(iris.StatusOK)
		fmt.Fprintf(c, "%v", res.FN)
	})

	i.Post(util.ImageRoute, func(c iris.Context) {
		start := time.Now()

		// 1. Execute the ImageMetadata function
		file, _, err := c.FormFile("image")
		if err != nil {
			c.StatusCode(iris.StatusBadRequest)
			return
		}
		defer file.Close()
		fileBytes, err := io.ReadAll(file)
		if err != nil {
			c.StatusCode(iris.StatusInternalServerError)
			return
		}
		res := client.Image(fileBytes)
		transformedImage := res.TransformedImage
		benchmarking.TimeTrack(start, "iris.imageHandler")

		// 2. Write the response
		c.Header("Content-Type", "application/octet-stream")
		_, err = c.Write(transformedImage)
		if err != nil {
			c.StatusCode(iris.StatusInternalServerError)
			return
		}
		return
	})

	i.Get(util.MathRoute, func(c iris.Context) {
		start := time.Now()
		// 1. Get the query parameter "n"
		nStr := c.URLParam("n")
		num, err := strconv.ParseInt(nStr, 10, 0)
		if err != nil {
			c.StatusCode(iris.StatusBadRequest)
			fmt.Fprintf(c, "invalid value for n: %s", nStr)
			return
		}

		// 2. Calculate Pi
		res := client.Math(num)

		benchmarking.TimeTrack(start, "iris.mathHandler")
		// 3. Write the response
		fmt.Fprintf(c, "%v", res.FN)
	})

	i.Get(util.SleepRoute, func(c iris.Context) {
		start := time.Now()
		// 1. Execute the Sleeper function
		res := client.Sleep()
		benchmarking.TimeTrack(start, "iris.sleepHandler")

		// 2. Write the response
		fmt.Fprintf(c, "%v", res.FN)
	})

	log.Info().Msg(fmt.Sprintf("Starting server on  %s:%d", host, port))
	log.Fatal().Err(i.Listen(fmt.Sprintf("%s:%d", host, port))).Msg("Failed to start server")
}
