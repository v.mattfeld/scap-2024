#!/bin/bash

LATEST_HASH=$(git rev-parse --short HEAD)
# Build the container
docker build -t valeriusm/scap:latest -t valeriusm/scap:$LATEST_HASH --platform=linux/amd64 --network host --no-cache .

# Check if the build was successful
if [ $? -eq 0 ]; then
    # Push the images
    docker push valeriusm/scap:latest
    docker push valeriusm/scap:$LATEST_HASH
else
    echo "Docker build failed, not pushing the image."
fi