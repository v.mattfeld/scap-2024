package fns

import (
	"os"

	"gopkg.in/gographics/imagick.v3/imagick"
)

func handleError(err error) error {
	if err != nil {
		return err
	}
	return nil
}

func TransformImage(inputImage []byte) ([]byte, error) {
	defer imagick.Terminate()

	mw := imagick.NewMagickWand()

	err := mw.ReadImageBlob(inputImage)
	if handleError(err) != nil {
		return nil, err
	}

	err = mw.BlurImage(0, 0.5)
	if handleError(err) != nil {
		return nil, err
	}

	err = mw.AddNoiseImage(imagick.NOISE_MULTIPLICATIVE_GAUSSIAN, 1)
	if handleError(err) != nil {
		return nil, err
	}

	err = mw.AddNoiseImage(imagick.NOISE_LAPLACIAN, 1)
	if handleError(err) != nil {
		return nil, err
	}

	err = mw.EnhanceImage()
	if handleError(err) != nil {
		return nil, err
	}

	err = mw.NegateImage(false)
	if handleError(err) != nil {
		return nil, err
	}

	err = mw.NormalizeImage()
	if handleError(err) != nil {
		return nil, err
	}

	// create temporary file
	file, err := os.CreateTemp("/data/tmp/", "scap_image_transform-*.out")
	if handleError(err) != nil {
		return nil, err
	}
	err = mw.WriteImageFile(file)
	if handleError(err) != nil {
		return nil, err
	}

	// read temporary file
	content, err := os.ReadFile(file.Name())
	if handleError(err) != nil {
		return nil, err
	}
	defer os.Remove(file.Name())

	return content, nil
}
