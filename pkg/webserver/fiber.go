package webserver

import (
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/benchmarking"
	"valerius.me/scap/pkg/fns/srpc"
	"valerius.me/scap/pkg/webserver/util"
)

func Fiber(host, rpcServerAddress string, port int) {
	f := fiber.New()
	client := srpc.NewRpcClient(rpcServerAddress)

	f.Get(util.EmptyRoute, func(c *fiber.Ctx) error {
		start := time.Now()
		// 1. Execute the Empty function
		res := client.Empty()

		// 2. Write the response
		c.Status(fiber.StatusOK)
		benchmarking.TimeTrack(start, "fiber.emptyHandler")
		return c.SendString(fmt.Sprintf("%v", res.FN))
	})

	// TODO: implement
	f.Post(util.ImageRoute, func(c *fiber.Ctx) error {
		start := time.Now()
		// 1. Execute the ImageMetadata function
		fileHeader, err := c.FormFile("image")
		if err != nil {
			c.Status(fiber.StatusBadRequest)
			return c.SendString("no image found in request")
		}
		file, err := fileHeader.Open()
		if err != nil {
			c.Status(fiber.StatusInternalServerError)
			return c.SendString("error opening file")
		}
		defer file.Close()
		fileBytes, err := io.ReadAll(file)
		if err != nil {
			c.Status(fiber.StatusInternalServerError)
			return c.SendString("error reading file")
		}
		res := client.Image(fileBytes)
		transformedImage := res.TransformedImage
		benchmarking.TimeTrack(start, "fiber.imageHandler")

		// 2. Write the response
		c.Set(fiber.HeaderContentType, fiber.MIMEOctetStream)
		return c.Send(transformedImage)
	})

	f.Get(util.MathRoute, func(c *fiber.Ctx) error {
		start := time.Now()
		// 1. Get the query parameter "n"
		nStr := c.Query("n")
		num, err := strconv.ParseInt(nStr, 10, 0)
		if err != nil {
			c.Status(fiber.StatusBadRequest)
			return c.SendString(fmt.Sprintf("invalid value for n: %s", nStr))
		}
		// 2. Calculate Pi
		res := client.Math(num)
		// 3. Write the response
		benchmarking.TimeTrack(start, "fiber.mathHandler")
		return c.SendString(fmt.Sprintf("%v", res.FN))
	})

	f.Get(util.SleepRoute, func(c *fiber.Ctx) error {
		start := time.Now()
		// 1. Execute the Sleeper function
		res := client.Sleep()
		benchmarking.TimeTrack(start, "fiber.sleepHandler")
		// 2. Write the response
		c.Status(fiber.StatusOK)
		return c.SendString(fmt.Sprintf("%v", res.FN))
	})

	log.Info().Msg(fmt.Sprintf("Starting server on  %s:%d", host, port))
	log.Fatal().Err(f.Listen(fmt.Sprintf("%s:%d", host, port))).Msg("Failed to start server")
}
