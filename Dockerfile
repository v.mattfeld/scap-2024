# Build stage
FROM ubuntu:noble AS build

# install build essentials and magickwand
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -q -y build-essential pkg-config libmagickwand-dev wget libmagickcore-dev \
    ca-certificates \
    libjpeg-dev libpng-dev libtiff-dev \
    libgif-dev libx11-dev --no-install-recommends

WORKDIR /imagemagick_install
RUN wget --no-check-certificate "https://github.com/ImageMagick/ImageMagick/archive/refs/tags/7.1.1-29.tar.gz" && \
    tar xvzf 7.1.1-29.tar.gz && \
      cd ImageMagick-7.1.1-29 && \
      ./configure \
          --without-magick-plus-plus \
          --without-perl \
          --disable-openmp \
          --with-gvc=no \
          --disable-docs && \
      make -j$(nproc) && make install && \
      ldconfig /usr/local/lib

# install go
WORKDIR /go_install
RUN wget --no-check-certificate "https://go.dev/dl/go1.22.1.linux-amd64.tar.gz"
RUN tar -xf "go1.22.1.linux-amd64.tar.gz"
RUN chown -R root:root ./go
RUN mv -v go /usr/local
ENV GOPATH /root/go
ENV PATH $PATH:/usr/local/go/bin:$GOPATH/bin

WORKDIR /scap
COPY . .

#RUN go install
RUN go mod tidy && go build -o /usr/local/bin/scap-bin ./cmd/main.go

# Final stage
FROM ubuntu:noble AS final

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -q -y build-essential pkg-config libmagickwand-dev wget libmagickcore-dev \
    libjpeg-dev libpng-dev libtiff-dev \
    libgif-dev libx11-dev --no-install-recommends

# Copy the binary from the build stage
COPY --from=build /usr/local/bin/scap-bin /usr/local/bin/scap-bin
COPY --from=build /usr/local/lib /usr/local/lib
RUN ldconfig /usr/local/lib

EXPOSE 80

CMD ["/usr/local/bin/scap-bin"]