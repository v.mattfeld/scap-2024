package main

import (
	"flag"

	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/fns/srpc"
	"valerius.me/scap/pkg/webserver"
)

var (
	mode             string
	rpcServerAddress string
	webServerAddress string
	webServerType    string
	webServerPort    int
)

func init() {
	flag.StringVar(&mode, "mode", "server", "Mode to run in. Either 'server' or 'rpc'")
	flag.StringVar(&mode, "m", "server", "Mode to run in. Either 'server' or 'rpc'")
	flag.StringVar(&rpcServerAddress, "rpc-server-address", "localhost:1234", "Address of the RPC server")
	flag.StringVar(&rpcServerAddress, "r", "localhost:1234", "Address of the RPC server")
	flag.StringVar(&webServerAddress, "web-server-address", "localhost", "Address of the web server")
	flag.StringVar(&webServerAddress, "w", "localhost", "Address of the web server")
	flag.StringVar(&webServerType, "web-server-type", "net_http", "Type of web server to use. One of 'echo', 'fiber', 'iris', 'gin', 'net_http'")
	flag.StringVar(&webServerType, "t", "net_http", "Type of web server to use. One of 'echo', 'fiber', 'iris', 'gin','net_http'")
	flag.IntVar(&webServerPort, "web-server-port", 80, "Port to run the web server on")
	flag.IntVar(&webServerPort, "p", 80, "Port to run the web server on")
}

func rpcMode(rpcServerAddress string) {
	log.Info().Str("mode", mode).Msg("Starting RPC Server")
	srpc.StartRPC(rpcServerAddress)
}

func serverMode(host, framework, rpcServerAddress string, port int) {
	log.Info().Str("mode", mode).Msg("Starting Web Server")
	switch framework {
	case "echo":
		webserver.Echo(host, rpcServerAddress, port)
	case "fiber":
		webserver.Fiber(host, rpcServerAddress, port)
	case "iris":
		webserver.Iris(host, rpcServerAddress, port)
	case "gin":
		webserver.Gin(host, rpcServerAddress, port)
	case "net_http":
		webserver.NetHttp(host, rpcServerAddress, port)
	default:
		log.Fatal().Str("framework", framework).Msg("Unknown or unimplemented framework")
	}
}

func main() {
	flag.Parse()
	if mode == "rpc" {
		rpcMode(rpcServerAddress)
	} else {
		serverMode(webServerAddress, webServerType, rpcServerAddress, webServerPort)
	}
}
