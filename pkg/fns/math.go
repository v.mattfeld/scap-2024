package fns

import (
	"fmt"
	"math"
	"math/rand"
)

// estimatePi is a function that estimates the value of Pi using the monte carlo method
func estimatePi(n int64) float64 {
	r := rand.New(rand.NewSource(42))
	pointsInsideCircle := 0

	for i := int64(0); i < n; i++ {
		x := r.Float64()
		y := r.Float64()
		distance := math.Sqrt(x*x + y*y)
		if distance <= 1 {
			pointsInsideCircle++
		}
	}

	pi := 4. * float64(pointsInsideCircle) / float64(n)

	return pi
}

// Math is a function that estimates the value of Pi using by calling the estimatePi function
// with a given number of points
// pointsStr is a string to outsource the parsing into this function from the webserver
func Math(points int64) (RPCResponse, error) {
	return RPCResponse{FN: fmt.Sprintf("%.5f", estimatePi(points))}, nil
}
