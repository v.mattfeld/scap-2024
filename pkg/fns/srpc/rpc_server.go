package srpc

import (
	"net"
	"net/http"
	"net/rpc"
	"valerius.me/scap/pkg/fns"

	"github.com/rs/zerolog/log"
)

// StartRPC starts the rpc server
func StartRPC(rpcServerAddress string) {
	rpc.Register(new(fns.Fn))
	rpc.HandleHTTP()
	l, err := net.Listen("tcp", rpcServerAddress)
	if err != nil {
		log.Fatal().Err(err).Msg("listen error")
	}
	log.Info().Msgf("Starting RPC server on %s", rpcServerAddress)
	if err := http.Serve(l, nil); err != nil {
		log.Fatal().Err(err).Msg("serve error")
	}
}
