package fns

import (
	"time"

	"github.com/gin-gonic/gin"
)

// Sleeper is a function that sleeps a given duration, defaults to one second
func Sleeper(d ...time.Duration) gin.H {
	var duration time.Duration
	if len(d) == 0 {
		duration = time.Second
	} else {
		duration = d[0]
	}
	time.Sleep(duration)
	return gin.H{"FN": "Sleep"}
}
