package webserver

import (
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/benchmarking"
	"valerius.me/scap/pkg/fns/srpc"
	"valerius.me/scap/pkg/webserver/util"
)

func Gin(host, rpcServerAddress string, port int) {
	g := gin.Default()
	client := srpc.NewRpcClient(rpcServerAddress)

	g.GET(util.EmptyRoute, func(ctx *gin.Context) {
		start := time.Now()
		// 1. Execute the Empty function
		res := client.Empty()
		benchmarking.TimeTrack(start, "gin.emptyHandler")
		// 2. Write the response
		ctx.String(200, res.FN)
	})

	g.POST(util.ImageRoute, func(ctx *gin.Context) {
		start := time.Now()
		// 1. Execute the ImageMetadata function
		fileHeader, err := ctx.FormFile("image")
		if err != nil {
			ctx.JSON(400, gin.H{"error": "no image found in request"})
			return
		}
		file, err := fileHeader.Open()
		if err != nil {
			ctx.JSON(500, gin.H{"error": "error opening file"})
			return
		}
		defer file.Close()
		fileBytes, err := io.ReadAll(file)
		if err != nil {
			ctx.JSON(500, gin.H{"error": "error reading file"})
			return
		}
		res := client.Image(fileBytes)
		transformedImage := res.TransformedImage
		benchmarking.TimeTrack(start, "gin.imageHandler")

		// 2. Write the response
		ctx.Data(200, "application/octet-stream", transformedImage)
	})

	g.GET(util.MathRoute, func(ctx *gin.Context) {
		start := time.Now()
		// 1. Get the query parameter "n"
		nStr := ctx.Query("n")
		// 2. Calculate Pi
		num, err := strconv.ParseInt(nStr, 10, 0)
		if err != nil {
			ctx.JSON(400, gin.H{"error": "invalid value for n"})
			return
		}

		res := client.Math(num)
		benchmarking.TimeTrack(start, "gin.mathHandler")
		// 3. Write the response
		ctx.String(200, res.FN)
	})

	g.GET(util.SleepRoute, func(ctx *gin.Context) {
		start := time.Now()
		// 1. Execute the Sleeper function
		res := client.Sleep()
		benchmarking.TimeTrack(start, "gin.sleepHandler")
		// 2. Write the response
		ctx.String(200, res.FN)
	})

	log.Info().Msg(fmt.Sprintf("Starting server on  %s:%d", host, port))
	log.Fatal().Err(g.Run(fmt.Sprintf("%s:%d", host, port))).Msg("Failed to start server")
}
