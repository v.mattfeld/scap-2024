package webserver

import (
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/benchmarking"
	"valerius.me/scap/pkg/fns/srpc"
	"valerius.me/scap/pkg/webserver/util"
)

func Echo(host, rpcServerAddress string, port int) {
	e := echo.New()
	client := srpc.NewRpcClient(rpcServerAddress)

	e.GET(util.EmptyRoute, func(ctx echo.Context) error {
		start := time.Now()
		// 1. Execute the Empty function
		res := client.Empty()
		benchmarking.TimeTrack(start, "echo.emptyHandler")
		// 2. Write the response
		return ctx.String(http.StatusOK, fmt.Sprintf("%v", res.FN))
	})

	e.POST(util.ImageRoute, func(ctx echo.Context) error {
		start := time.Now()
		// 1. Execute the ImageMetadata function
		fileHeader, err := ctx.FormFile("image")
		if err != nil {
			return ctx.String(http.StatusBadRequest, "no image found in request")
		}
		file, err := fileHeader.Open()
		if err != nil {
			return ctx.String(http.StatusInternalServerError, "error opening file")
		}
		defer file.Close()
		fileBytes, err := io.ReadAll(file)
		if err != nil {
			return ctx.String(http.StatusInternalServerError, "error reading file")
		}
		res := client.Image(fileBytes)
		transformedImage := res.TransformedImage
		benchmarking.TimeTrack(start, "echo.imageHandler")

		// 2. Write the response
		return ctx.Blob(http.StatusOK, "application/octet-stream", transformedImage)
	})

	e.GET(util.MathRoute, func(ctx echo.Context) error {
		start := time.Now()
		// 1. Get the query parameter "n"
		nStr := ctx.QueryParam("n")
		num, err := strconv.ParseInt(nStr, 10, 0)
		if err != nil {
			ctx.String(http.StatusBadRequest, fmt.Sprintf("invalid value for n: %s", nStr))
			return err
		}
		// 2. Calculate Pi
		res := client.Math(num)
		// 3. Write the response
		benchmarking.TimeTrack(start, "echo.mathHandler")
		return ctx.String(http.StatusOK, fmt.Sprintf("%v", res.FN))
	})

	e.GET(util.SleepRoute, func(ctx echo.Context) error {
		start := time.Now()
		// 1. Execute the Sleeper function
		res := client.Sleep()
		// 2. Write the response
		benchmarking.TimeTrack(start, "echo.sleepHandler")
		return ctx.String(http.StatusOK, fmt.Sprintf("%v", res.FN))
	})

	log.Info().Msg(fmt.Sprintf("Starting server on  %s:%d", host, port))
	log.Fatal().Err(e.Start(fmt.Sprintf("%s:%d", host, port))).Msg("Failed to start server")
}
