package webserver

import (
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"
	"valerius.me/scap/pkg/benchmarking"
	"valerius.me/scap/pkg/fns/srpc"
	"valerius.me/scap/pkg/webserver/util"
)

type Nh struct {
	client *srpc.RpcClient
}

// NetHttp
func NetHttp(host, rpcServerAddress string, port int) {
	nh := new(Nh)
	nh.client = srpc.NewRpcClient(rpcServerAddress)

	// Define the routes with their handlers
	http.HandleFunc(util.EmptyRoute, nh.emptyHandler)
	http.HandleFunc(util.ImageRoute, nh.imageHandler)
	http.HandleFunc(util.MathRoute, nh.mathHandler)
	http.HandleFunc(util.SleepRoute, nh.sleepHandler)

	// Start the server
	log.Info().Msg(fmt.Sprintf("Starting server on  %s:%d", host, port))
	log.Fatal().Err(http.ListenAndServe(fmt.Sprintf("%s:%d", host, port), nil)).Msg("Failed to start server")
}

func (n *Nh) emptyHandler(w http.ResponseWriter, _ *http.Request) {
	start := time.Now()
	// 1. Execute the Empty function
	res := n.client.Empty()
	benchmarking.TimeTrack(start, "nh.emptyHandler")

	// 2. Write the response
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%v", res.FN)
}

func (n *Nh) imageHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	// 1. Execute the ImageMetadata function
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	// get file from request
	file, _, err := r.FormFile("image")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer file.Close()

	// get bytes from file
	fileBytes, err := io.ReadAll(file)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// send bytes to rpc server
	res := n.client.Image(fileBytes)

	transformedImageBytes := res.TransformedImage
	benchmarking.TimeTrack(start, "nh.imageHandler")

	w.Header().Set("Content-Type", "application/octet-stream")
	// 2. Write the response
	_, err = w.Write(transformedImageBytes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	return
}

func (n *Nh) mathHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	// 1. Get the query parameter "n"
	nStr := r.URL.Query().Get("n")

	num, err := strconv.ParseInt(nStr, 10, 0)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// 2. Calculate Pi
	res := n.client.Math(num)

	benchmarking.TimeTrack(start, "nh.mathHandler")
	// 3. Write the response
	fmt.Fprintf(w, "%v", res.FN)
}

func (n *Nh) sleepHandler(w http.ResponseWriter, _ *http.Request) {
	start := time.Now()
	// 1. Execute the Sleeper function
	res := n.client.Sleep()

	benchmarking.TimeTrack(start, "nh.sleepHandler")
	// 2. Write the response
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%v", res.FN)
}
